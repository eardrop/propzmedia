# Propzmedia

Для тестов решил использовать новый для меня [Jest](https://jestjs.io/).
Покрыть тестами и заодно познакомится с новой технологией.

## Build Setup

``` bash
$ npm install
```

## Run Tests

``` bash
$ npm test
```
