function getType (v) {
	if (typeof v === 'undefined') return 'undefined'
	if (v === null) return 'null'
	if (v instanceof Array) return 'array'
	if (
		(!v.hasOwnProperty('length') && v.length >= 0) ||
		typeof v === "object" &&
		typeof (v.length) === "number" &&
		(v.length === 0 || (v.length > 0 && (v.length - 1) in v))
	) return 'array-like'
	return typeof v
}

module.exports = getType
