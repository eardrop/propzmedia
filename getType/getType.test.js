const getType = require('./getType');

test('getType (undefined)', () => {
  var a
  expect(getType()).toBe('undefined')
  expect(getType(a)).toBe('undefined')
})

test('getType boolean', () => {
  expect(getType(true)).toBe('boolean')
  expect(getType(false)).toBe('boolean')
})

test('getType null', () => {
  expect(getType(null)).toBe('null')
})

test('getType number', () => {
  expect(getType(-999999999999999)).toBe('number')
  expect(getType(0)).toBe('number')
  expect(getType('1')).toBe('string')
  expect(getType(99)).toBe('number')
})

test('getType string', () => {
  expect(getType('')).toBe('string')
  expect(getType('Lorem')).toBe('string')
})

test('getType function', () => {
  expect(getType(function() {})).toBe('function')
})

test('getType Array', () => {
  var arr = [1, 'Lorem ipsum', {a: 'a', b: 'b'}]
  expect(getType([])).toBe('array')
  expect(getType(arr)).toBe('array')
})

test('getType array-like', () => {
  var arr = {}
  var i = 0
  while (i < 5) {
    arr[i] = i * i
    i++
  }
  arr.length = i
  expect(getType(arr)).toBe('array-like')
})

test('getType Object', () => {
  expect(getType({})).toBe('object')
})
