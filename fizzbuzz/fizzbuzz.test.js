const fizzbuzz = require('./fizzbuzz');

test('fizzbuzz (no params) function', () => {
  let fb = fizzbuzz()
  expect(fb()).toBe('FizzBuzz')
  expect(fb()).toBe(1)
  expect(fb()).toBe(2)
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe(4)
  expect(fb()).toBe('Buzz')
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe(7)
  expect(fb()).toBe(8)
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe('Buzz')
  expect(fb()).toBe(11)
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe(13)
  expect(fb()).toBe(14)
  expect(fb()).toBe('FizzBuzz')
});

test('fizzbuzz (0, 3) function', () => {
  let fb = fizzbuzz(0, 3)
  for (let i = 0; i < 1000; i+=3) {
    expect(fb()).toBe(i % 5 ? 'Fizz' : 'FizzBuzz')
  }
})

test('fizzbuzz (0, 5) function', () => {
  let fb = fizzbuzz(0, 5)
  for (let i = 0; i < 1000; i+=5) {
    expect(fb()).toBe(i % 3 ? 'Buzz' : 'FizzBuzz')
  }
})

test('fizzbuzz (-100, -22) function', () => {
  let fb = fizzbuzz(-100, -22)
  expect(fb()).toBe('Buzz')
  expect(fb()).toBe(-122)
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe(-166)
  expect(fb()).toBe(-188)
  expect(fb()).toBe('FizzBuzz')
  expect(fb()).toBe(-232)
  expect(fb()).toBe(-254)
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe(-298)
  expect(fb()).toBe('Buzz')
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe(-364)
  expect(fb()).toBe(-386)
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe('Buzz')
})

test('fizzbuzz (9999999999999, 48712) function', () => {
  let fb = fizzbuzz(9999999999999, 48712)
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe(10000000048711)
  expect(fb()).toBe(10000000097423)
  expect(fb()).toBe('FizzBuzz')
  expect(fb()).toBe(10000000194847)
  expect(fb()).toBe(10000000243559)
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe(10000000340983)
  expect(fb()).toBe('Buzz')
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe(10000000487119)
  expect(fb()).toBe(10000000535831)
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe('Buzz')
  expect(fb()).toBe(10000000681967)
  expect(fb()).toBe('Fizz')
  expect(fb()).toBe(10000000779391)
})
