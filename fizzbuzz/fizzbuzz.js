function sequenceFizzBuzz(index = 0, step = 1) {
  return function() {
    var fizzbuzz = (index % 3 ? '' : 'Fizz') + (index % 5 ? '' : 'Buzz') || index
    index += step
    return fizzbuzz
  }
}

module.exports = sequenceFizzBuzz
